#include "stm32f10x.h"
#include "eprintf.h"
#include "adc.h"
#include "led.h"
#include "time.h"
#include "uart.h"
#include "nrf24l01.h"
#include "spi.h"



#ifndef STM32_MINIKIT_48
static void prvClockInit(void);
#endif


int main(void)
{
	static unsigned char  tx_buf[32] = {0};
	unsigned char key = 0;
	unsigned char PA0 = 0, PA1 = 0, PA2 = 0, PA3 = 0;
    ledInit();
    timeInit();
	spiInit();
    timeDelayMS(100);

    uartInit(115200);
    eprintf("uart init?\n");


	Joystick_Init();
	Joystick_Configuration();
	//timeDelayMS(100);
/*	
	while(1)	
	{
		Joystick_Show();
		//Joystick_Getdata(PA0, PA1, PA2, PA3);
		eprintf("\n");
		timeDelayMS(100);	
	}	
*/
	while(1)
	{
		eprintf("\nTX\n");
		PA0 = Joystick_Getdata_PA0();
		PA1 = Joystick_Getdata_PA1();
		PA2 = Joystick_Getdata_PA2();
		PA3 = Joystick_Getdata_PA3();		
		//Joystick_Getdata(PA0, PA1, PA2, PA3);
		TX_Mode();
		key++;
		timeDelayMS(100);
		if(NRF24L01_TxPacket(tx_buf)==0x20)
		{
			ledReverse();
			eprintf("\n\r tx sucess\n");
/*
    		for(t=0;t<32;t++)
	    	{
    				  tx_buf[t]=t+key;	
	    	}
*/
			tx_buf[0] = 'F';
			tx_buf[1] = 'o';
			tx_buf[2] = 'u';
			tx_buf[3] = 'r';
			tx_buf[4] = 'A';
			tx_buf[5] = 0;
			tx_buf[6] = PA0;
			tx_buf[7] = PA1;
			tx_buf[8] = PA2;
			tx_buf[9] = PA3;

			eprintf("\n\r check: %s\n", tx_buf);
			eprintf("\n\r data: %d, %d, %d, %d, \n", tx_buf[6], tx_buf[7], tx_buf[8], tx_buf[9]);
		}

		else
		{
				//ledReverse();
			eprintf("\n\r check tx\n");
			timeDelayMS(500);
				
		}
		
	}

    return 0;
}

#ifndef STM32_MINIKIT_48
//Clock configuration
static void prvClockInit(void)
{
  ErrorStatus HSEStartUpStatus;

  RCC_DeInit();
  /* Enable HSE */
  RCC_HSEConfig(RCC_HSE_ON);
  /* Wait till HSE is ready */
  HSEStartUpStatus = RCC_WaitForHSEStartUp();

  if (HSEStartUpStatus == SUCCESS)
  {
    /* Enable Prefetch Buffer */
    FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);

    /* Flash 2 wait state */
    FLASH_SetLatency(FLASH_Latency_2);

    /* HCLK = SYSCLK */
    RCC_HCLKConfig(RCC_SYSCLK_Div1);

    /* PCLK2 = HCLK */
    RCC_PCLK2Config(RCC_HCLK_Div1);

    /* PCLK1 = HCLK/2 */
    RCC_PCLK1Config(RCC_HCLK_Div2);

    /* ADCCLK = PCLK2/6 = 72 / 6 = 12 MHz*/
    RCC_ADCCLKConfig(RCC_PCLK2_Div6);

    /* PLLCLK = 16MHz/2 * 9 = 72 MHz */
    RCC_PLLConfig(RCC_PLLSource_HSE_Div2, RCC_PLLMul_9);

    /* Enable PLL */
    RCC_PLLCmd(ENABLE);

    /* Wait till PLL is ready */
    while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);

    /* Select PLL as system clock source */
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

    /* Wait till PLL is used as system clock source */
    while(RCC_GetSYSCLKSource() != 0x08);
  }
  else
  {
    GPIO_InitTypeDef GPIO_InitStructure;

    //Cannot start the main oscillator: red LED of death...
    ledInit();
    ledSet(1);

    //Cannot start xtal oscillator!
    while(1);
  }
}
#endif

