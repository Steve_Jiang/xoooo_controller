#ifndef _EPRINTF_H_
#define _EPRINTF_H_
#include <stdarg.h>

int eprintf(char * fmt, ...);


#endif