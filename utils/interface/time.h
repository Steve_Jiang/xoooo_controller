#ifndef _TIME_H_
#define _TIME_H_

void timeInit(void);

uint32_t timeGetMS(void);
uint64_t timeGetUS(void);

void timeDelayUS(uint32_t delay);
void timeDelayMS(uint32_t delay);


#endif
