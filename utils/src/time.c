#include "stm32f10x.h"
#include "time.h"

static struct
{
    volatile uint32_t msPeriod;
    uint32_t ticksPerUs;
    uint32_t ticksPerMs;
    uint32_t msPerPeriod;
}time;


void timeInit(void)
{
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    time.msPeriod = 0;
    time.ticksPerUs = SystemCoreClock / 1e6;
    time.ticksPerMs = SystemCoreClock / 1e3;
    time.msPerPeriod = 10;
    SysTick_Config(SystemCoreClock/(1000/time.msPerPeriod));

}

void SysTick_Handler(void)
{
    time.msPeriod += time.msPerPeriod;
}


uint32_t timeGetMS(void)
{
 	return time.msPeriod + (SysTick->LOAD - SysTick->VAL) / time.ticksPerMs;
}

uint64_t timeGetUS(void)
{
    return time.msPeriod * (uint64_t)1000 + (SysTick->LOAD - SysTick->VAL) / time.ticksPerUs;
}


void timeDelayUS(uint32_t delay)
{
    uint64_t target = timeGetUS() + delay - 2;
    while(timeGetUS() <= target)
        ;
}


void timeDelayMS(uint32_t delay)
{
    timeDelayUS(delay*1000);
}

