#include <ctype.h>
#include "eprintf.h"
#include "uart.h"

#define putcf uartPutc

static const char digit[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                             'A', 'B', 'C', 'D', 'E', 'F'};

static int itoa(int num, int base)
{
  long long int i=1;
  int len=0;
  unsigned int n = num;

  if (num==0)
  {
    putcf('0');
    return 1;
  }

  if(base==10 && num<0)
  {
    n = -num;

    putcf('-');
    len++;
  }

  while (n/i)
    i*=base;

  while(i/=base)
  {
    putcf(digit[(n/i)%base]);
    len++;
  }

  return len;
}

int evprintf(char * fmt, va_list ap)
{
  int len=0;
  float num;
  char* str;

  while (*fmt)
  {
    if (*fmt == '%')
    {
      while(!isalpha((unsigned)*++fmt));//TODO: Implement basic print length handling!
      switch (*fmt++)
      {
        case 'i':
        case 'd':
          len += itoa(va_arg( ap, int ), 10);
          break;
        case 'x':
        case 'X':
          len += itoa(va_arg( ap, int ), 16);
          break;
        case 'f':
          num = va_arg(ap, double );
          len += itoa((int)num, 10);
          putcf('.'); len++;
          if(num<0) num=-num;
          len += itoa((num-(int)num)*10e5, 10);
          break;
        case 's':
          str = va_arg( ap, char* );
          while(*str)
          {
            putcf(*str++);
            len++;
          }
          break;
        default:
          break;
      }
    }
    else
    {
      if(*fmt == '\n')
        putcf('\r');
      putcf(*fmt++);
      len++;
    }
  }

  return len;
}

int eprintf(char * fmt, ...)
{
  va_list ap;
  int len;

  va_start(ap, fmt);
  len = evprintf(fmt, ap);
  va_end(ap);

  return len;
}