#xoooo firmware Makefile

DEBUG ?= 1
CROSS_COMPILE ?= arm-none-eabi-

AS = $(CROSS_COMPILE)as
CC = $(CROSS_COMPILE)gcc
LD = $(CROSS_COMPILE)gcc
SIZE = $(CROSS_COMPILE)size
OBJCOPY = $(CROSS_COMPILE)objcopy



#CM3
VPATH += lib/CMSIS/Core/CM3
VPATH += lib/CMSIS/Core/CM3/startup/gcc
CM3_INCLUDES = -Ilib/CMSIS/Core/CM3
CM3_OBJ = startup_stm32f10x_md.o core_cm3.o system_stm32f10x.o
CM3_FLAGS = 


#ST lib
VPATH += lib/STM32F10x_StdPeriph_Driver/src/
ST_INCLUDES = -Ilib/STM32F10x_StdPeriph_Driver/inc
ST_OBJ += misc.o
ST_OBJ += stm32f10x_adc.o
#ST_OBJ+=stm32f10x_bkp.o
#ST_OBJ+=stm32f10x_can.o
#ST_OBJ+=stm32f10x_crc.o
#ST_OBJ+=stm32f10x_dac.o
ST_OBJ+=stm32f10x_dbgmcu.o
ST_OBJ+=stm32f10x_dma.o
ST_OBJ+=stm32f10x_exti.o
ST_OBJ+=stm32f10x_flash.o
#ST_OBJ+=stm32f10x_fsmc.o
ST_OBJ+=stm32f10x_gpio.o
ST_OBJ+=stm32f10x_i2c.o
ST_OBJ+=stm32f10x_iwdg.o
#ST_OBJ+=stm32f10x_pwr.o
ST_OBJ+=stm32f10x_rcc.o
#ST_OBJ+=stm32f10x_rtc.o
#ST_OBJ+=stm32f10x_sdio.o
ST_OBJ+=stm32f10x_spi.o
ST_OBJ+=stm32f10x_tim.o
ST_OBJ+=stm32f10x_usart.o
#ST_OBJ+=stm32f10x_wwdg.o
ST_FLAGS = -DSTM32F10X_MD -include stm32f10x_conf.h


#xooo obj
VPATH += controler/src
XOOO_INCLUDES += -Icontroler/interface 
#XOOO_OBJ += engine.o communication.o main_remoter.o nrf24l01p.o
XOOO_FLAGS = 

#xooo obj
VPATH += src drivers/src utils/src
XOOO_INCLUDES += -Isrc -Idrivers/interface -Iutils/interface
XOOO_OBJ += main.o
XOOO_OBJ += led.o uart.o nrf24l01.o spi.o adc.o time.o
XOOO_OBJ += eprintf.o
XOOO_FLAGS = 


#################################################
PROCESSOR = -mcpu=cortex-m3 -mthumb 
OBJ = $(CM3_OBJ) $(ST_OBJ) $(XOOO_OBJ)
INCLUDES+= -Iconfig $(CM3_INCLUDES) $(ST_INCLUDES) $(XOOO_INCLUDES)
CFLAGS += $(PROCESSOR) $(CM3_FLAGS) $(ST_FLAGS) $(XOOO_FLAGS)
CFLAGS += $(INCLUDES) -Wall -fno-strict-aliasing
CFLAGS += $(PROCESSOR) -ffunction-sections -fdata-sections
ifeq ($(DEBUG), 1)
  CFLAGS += -O0 -g3
else
  CFLAGS += -Os -g3
endif

CFLAGS += -DSTM32_MINIKIT_48

LDFLAGS = $(PROCESSOR) -Wl,-Map,$(PROG).map,--cref,--gc-sections
LDFLAGS += -T scripts/STM32F103_32K_20K_FLASH.ld
ASFLAGS = $(PROCESSOR) $(INCLUDES)


########################################
PROG = xoooo
BUILD = build
VPATH += $(BUILD)



all: build
build: $(PROG).hex $(PROG).bin



CC_COMMAND=$(CC) $(CFLAGS) -c $< -o $(BUILD)/$@
.c.o: 
#	@echo $(CC_COMMAND)
	@echo CC $@
	@$(CC_COMMAND)

LD_COMMAND=$(LD) $(LDFLAGS) $(foreach o,$(OBJ),$(BUILD)/$(o)) -o $@
$(PROG).elf: $(OBJ)
#	@echo $(LD_COMMAND)
	@echo LD $@
	@$(LD_COMMAND)

HEX_COMMAND=$(OBJCOPY) $< -O ihex $@
$(PROG).hex: $(PROG).elf
#	@echo $(HEX_COMMAND)
	@$(HEX_COMMAND)

BIN_COMMAND=$(OBJCOPY) $< -O binary --pad-to 0 $@
$(PROG).bin: $(PROG).elf
#	@echo $(BIN_COMMAND)
	@$(BIN_COMMAND)

AS_COMMAND=$(AS) $(ASFLAGS) $< -o $(BUILD)/$@
.s.o:
#	@echo $(AS_COMMAND)
	@$(AS_COMMAND)

clean:
	@rm -f $(PROG).elf $(PROG).hex $(PROG).bin $(PROG).map
	@rm -f $(foreach o,$(OBJ),$(BUILD)/$(o))
	@echo clean all done

##########
