#include "stm32f10x.h"
#include "spi.h"
#include "nrf24l01.h"
#include "eprintf.h"
#include "time.h"

#define __DEBUG 0


#define PAYLOADSIZE 5
#define TX_PLOAD_WIDTH  32
#define RX_PLOAD_WIDTH  32


unsigned char TX_ADDRESS[5]={0x34,0x43,0x10,0x10,0x01}; //发送地址
unsigned char RX_ADDRESS[5]={0x34,0x43,0x10,0x10,0x01}; //发送地址	


#define BV(n) (1<<(n))


/*
 * command。*/
#define RF_CMD_R_REGISTER       0x00/*0b00000000*/  /* 读寄存器，0b000aaaaa，a为地址。 */
#define RF_CMD_W_REGISTER       0x20/*0b00100000*/  /* 写寄存器，0b001aaaaa，a为地址。 */
    #define RF_CMD_RWR_MASK     0x1F/*0b00011111*/  /* 寄存器地址掩码，0bcccaaaaa */
#define RF_CMD_R_RX_PAYLOAD     0x61/*0b01100001*/  /* 获取数据。 */
#define RF_CMD_W_TX_PAYLOAD     0xA0/*0b10100000*/  /* 推送数据。 */
#define RF_CMD_FLUSH_TX         0xE1/*0b11100001*/  /* 清空发送缓存。 */
#define RF_CMD_FLUSH_RX         0xE2/*0b11100010*/  /* 清空接收缓存。 */
#define RF_CMD_REUSE_TX_PL      0xE3
#define RF_CMD_ACTIVATE         0x50
#define RF_CMD_R_RX_PL_WID      0x60/*0b01100000*/  /* 获取接收到的数据的长度。 */
#define RF_CMD_W_ACK_PAYLOAD(P) (0xA8|(P&0x0F))
#define RF_CMD_W_PAYLOAD_NO_ACK 0xD0
#define RF_CMD_NOP              0xFF/*0b11111111*/  /* 空操作，用来读STATUS寄存器。 */

#define ACTIVATE_DATA   0x73

/*
 * regester map*/
#define RF_REG_CONFIG 0x00
    #define RF_REG_CONFIG_MASK_RX_DR    BV(6) /* 接收中断。 */
    #define RF_REG_CONFIG_MASK_TX_DS    BV(5) /* 发送中断。 */
    #define RF_REG_CONFIG_MASK_MAX_RT   BV(4) /* 重发失败中断。 */
    #define RF_REG_CONFIG_EN_CRC        BV(3) /* 使用CRC。 */
    #define RF_REG_CONFIG_CRCO          BV(2) /* 使用2位CRC。 */
    #define RF_REG_CONFIG_PWR_UP        BV(1) /* 马上进入Standby1状态。 */
    #define RF_REG_CONFIG_PRIM_RX       BV(0) /* 是否作为接收器。 */
    #define RF_REG_CONFIG_DEFAULT_R        /* 默认。 */ \
        (RF_REG_CONFIG_CRCO | RF_REG_CONFIG_EN_CRC | \
        RF_REG_CONFIG_PWR_UP | RF_REG_CONFIG_PRIM_RX)
    #define RF_REG_CONFIG_DEFAULT_T        /* 默认。 */ \
        (RF_REG_CONFIG_CRCO | RF_REG_CONFIG_EN_CRC | \
        RF_REG_CONFIG_PWR_UP)
#define RF_REG_EN_AA 0x01
    #define RF_REG_EN_AA_ENAA_P0    BV(0) /* 0通道自动ACK。 */
    #define RF_REG_EN_AA_DEFAULT    RF_REG_EN_AA_ENAA_P0 /* 默认。 */
#define RF_REG_EN_RXADDR 0x02
    #define RF_REG_EN_RXADDR_ERX_P0 BV(0) /* 使用0通道。 */
    #define RF_REG_EN_RXADDR_DEFAULT RF_REG_EN_RXADDR_ERX_P0 /* 默认。 */
#define RF_REG_SETUP_AW 0x03
    #define RF_REG_SETUP_AW_N(n)    (n-2)  /* 使用n位地址，n:[3,5] */
    #define RF_REG_SETUP_AW_DEFAULT RF_REG_SETUP_AW_N(4) /* 默认 */
#define RF_REG_SETUP_RETR 0x04
    #define RF_REG_SETUP_RETR_ARD(n)    ((unsigned char)n<<4)   /* (n+1)*250μs没收到ACK就重发。 */
    #define RF_REG_SETUP_RETR_ARC(n)    ((unsigned char)n&0x0F) /* 最多重发n次。 */
    #define RF_REG_SETUP_RETR_DEFAULT   /* 默认 */ \
        (RF_REG_SETUP_RETR_ARD(3) | RF_REG_SETUP_RETR_ARC(4))
#define RF_REG_RF_CH 0x05
    #define RF_REG_RF_CH_MHZ(n)      ((unsigned char)(n-2400)&(~BV(7))) /* 设置频率，Mhz，[2400,2525]。*/
    #define RF_REG_RF_CH_DEFAULT     RF_REG_RF_CH_MHZ(2400) /* 默认 */
#define RF_REG_RF_SETUP 0x06
    #define RF_REG_RF_SETUP_CONT_WAVE   BV(7) /* 允许连续发送。 */
    #define RF_REG_RF_SETUP_RATE_250K   BV(5) /* 设置速率，250kBps。 */
    #define RF_REG_RF_SETUP_RATE_1M     0      /* 设置速率，1MBps。 */
    #define RF_REG_RF_SETUP_RATE_2M     BV(3) /* 设置速率，2MBps。 */
    #define RF_REG_RF_SETUP_PWR(n)      (n<<1) /* 设置发射功率，(-6*(3-n))dBm。 */
    #define RF_REG_RF_SETUP_DEFAULT     /* 默认 */ \
        (RF_REG_RF_SETUP_RATE_2M | RF_REG_RF_SETUP_PWR(3))
#define RF_REG_STATUS 0x07
    #define RF_REG_STATUS_RX_DR     BV(6) /* 接收到数据。 */
    #define RF_REG_STATUS_TX_DS     BV(5) /* 发送完数据。 */
    #define RF_REG_STATUS_MAX_RT    BV(4) /* 重发超过最大次数。 */
    #define RF_REG_STATUS_TX_FULL   BV(0) /* 发送缓冲区满。 */
#define RF_REG_FIFO_STATUS 0x17
    #define RF_REG_FIFO_STATUS_TX_REUSE     BV(6) /*  */
    #define RF_REG_FIFO_STATUS_TX_FULL      BV(5) /* 1:发送队列满。 */
    #define RF_REG_FIFO_STATUS_TX_EMPTY     BV(4) /* 1:发送队列空。 */
    #define RF_REG_FIFO_STATUS_RX_FULL      BV(1) /* 1:接收队列满。 */
    #define RF_REG_FIFO_STATUS_RX_EMPTY     BV(0) /* 1:接收队列空。 */
#define RF_REG_DYNPD 0x1C
    #define RF_REG_DYNPD_DPL_P0 BV(0) /* 设置通道0为动态数据长度。 */
    #define RF_REG_DYNPD_DEFAULT RF_REG_DYNPD_DPL_P0 /* 默认。 */
#define RF_REG_FEATURE 0x1D
    #define RF_REG_FEATURE_EN_DPL   BV(2) /* 允许动态数据长度。 */
    #define RF_REG_FEATURE_DEFAULT RF_REG_FEATURE_EN_DPL /* 默认。 */
#define RF_REG_RX_ADDR_P0 0x0A /* 接收的地址 */
#define RF_REG_TX_ADDR    0x10 /* 发送目的地址 */
#define RF_REG_RX_PW_P0   0x11 /* 0接收通道收到的数据的长度。 */

#define VAL_RF_SETUP_250K 0x26
#define VAL_RF_SETUP_1M   0x06
#define VAL_RF_SETUP_2M   0x0E

#define VAL_SETUP_AW_3B 1
#define VAL_SETUP_AW_4B 2
#define VAL_SETUP_AW_5B 3


int nrfInit(void)
{
    //eprintf("nrf 1\n");
    spiInit();
    //
    return 0;
}


void RX_Mode(void)
{
	timeDelayMS(30);
	RADIO_DIS_CE();

	nrfWriteBuf(RF_CMD_W_REGISTER+RF_REG_RX_ADDR_P0, RX_ADDRESS, 5);
	nrfWriteBuf(RF_CMD_W_REGISTER+RF_REG_TX_ADDR, TX_ADDRESS, 5);
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_EN_AA, 0x00);   // No Auto Acknoledgement
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_EN_RXADDR, 0x01);  // Enable data pipe 0 or 1??
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_RF_CH, 0x40);      // Channel 8
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_RX_PW_P0, RX_PLOAD_WIDTH);   // bytes of data payload for pipe 1
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_RF_SETUP, 0x0f); // 1Mbps	
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_CONFIG, 0x0f);

	RADIO_EN_CE();
	timeDelayMS(30);

#if __DEBUG
	 char buf2[5] = {0};
	 int i, ret = 0;
	 unsigned char aa = 0, rx = 0, retr = 0, ch = 0, setup = 0, config = 0;

	nrfReadBuff(RF_REG_TX_ADDR, buf2, 5);//RF_REG_TX_ADDR
    eprintf("buf2.1");
    for(i=0;i<5;i++)
    {
        eprintf(" %x", buf2[i]);
    }
    eprintf("\n");

	nrfReadBuff(RF_REG_RX_ADDR_P0, buf2, 5);//RF_REG_TX_ADDR
    eprintf("buf2.2");
    for(i=0;i<5;i++)
    {
        eprintf(" %x", buf2[i]);
    }
    eprintf("\n");

	aa = nrfReadReg(RF_REG_EN_AA);
	rx = nrfReadReg(RF_REG_EN_RXADDR);
	retr = nrfReadReg(RF_REG_SETUP_RETR);
	ch = nrfReadReg(RF_REG_RF_CH);
	setup = nrfReadReg(RF_REG_RF_SETUP);
	config = nrfReadReg(RF_REG_CONFIG);
	eprintf("aa:%x, rx:%x, retr:%x, ch:%x, setup:%x, config:%x\n",aa,rx,retr,ch,setup,config);
#endif
}

void TX_Mode(void)
{
	timeDelayMS(30);
	RADIO_DIS_CE();
	

	nrfWriteBuf(RF_CMD_W_REGISTER+RF_REG_TX_ADDR, (unsigned char*)TX_ADDRESS, 5);
	nrfWriteBuf(RF_CMD_W_REGISTER+RF_REG_RX_ADDR_P0, (unsigned char*)RX_ADDRESS, 5);	
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_EN_AA, 0x00);   //  to do
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_EN_RXADDR, 0x00);  //to do
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_SETUP_RETR, 0x00); // tp dp
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_RF_CH, 0x40); // to do 0x40  
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_RX_PW_P0, RX_PLOAD_WIDTH);
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_RF_SETUP, 0x0f); // 1Mbps	
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_CONFIG, 0x0e);

	RADIO_EN_CE();

}

unsigned char NRF24L01_RxPacket(unsigned char *rxbuf)
{
	unsigned char state = 0;
	//eprintf("Read status!\n");
	state = nrfReadReg(RF_CMD_R_REGISTER+RF_REG_STATUS);
	eprintf("\n\r rx state %x\n", state);

	if(state&0x40)
	{
		nrfReadBuff(RF_CMD_R_RX_PAYLOAD, rxbuf, RX_PLOAD_WIDTH);
		nrfWriteReg(RF_CMD_FLUSH_RX, 0xff);
		nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_STATUS, state);
		return 0;
	}
	return 1;
}

unsigned char NRF24L01_TxPacket(unsigned char *txbuf)
{
	unsigned char state = 0;
	RADIO_DIS_CE();
	nrfWriteBuf(RF_CMD_W_TX_PAYLOAD, txbuf, TX_PLOAD_WIDTH);//TX_PLOAD_WIDTH
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_CONFIG, RF_REG_CONFIG_DEFAULT_T);
	timeDelayMS(30);
 	RADIO_EN_CE();
	state = nrfReadReg(RF_REG_STATUS);
	//eprintf("\n\rTX state2 = %x\n", state);
	nrfWriteReg(RF_CMD_W_REGISTER+RF_REG_STATUS, state);
	
	//nrfWrite1Reg(RF_REG_STATUS, 0xFF);
	if(state&0x10)
	{
		nrfWriteReg(RF_CMD_FLUSH_RX, 0xff);;
		return 0x10;
	}
	if(state&0x20)
	{
		return 0x20;
	}
	
	return 0xff;
}





/* Read len bytes from a nRF24L register. 5 Bytes max */
unsigned char nrfReadReg(unsigned char address)
{
  unsigned char reg_val;

  RADIO_EN_CS();

  /* Send the read command with the address */
  spiSendByte( address );
  reg_val=spiReceiveByte();

  RADIO_DIS_CS();

  return reg_val;
}

unsigned char nrfReadBuff(unsigned char address, unsigned char *buffer, int len)
{
  unsigned char status;
  int i;

  RADIO_EN_CS();

  /* Send the read command with the address */
  status = spiSendByte( address );
  //eprintf("\t\n RX data: ");
  /* Read LEN bytes */
  for(i=0; i<len; i++)
  {
    buffer[i]=spiReceiveByte();
	//eprintf(" 0x%x", buffer[i]);
  }

  RADIO_DIS_CS();

  return status;
}


/* Write len bytes a nRF24L register. 5 Bytes max */
unsigned char nrfWriteReg(unsigned char address, char value)
{
  unsigned char status;

  RADIO_EN_CS();

  /* Send the write command with the address */
  status = spiSendByte( address );
  /* Write LEN bytes */
  spiSendByte(value);

  RADIO_DIS_CS();

  return status;
}

/* Write len bytes a nRF24L register. 5 Bytes max */
unsigned char nrfWriteBuf(unsigned char address, unsigned char *buffer, int len)
{
  unsigned char status;
  int i;

  RADIO_EN_CS();

  /* Send the write command with the address */
  status = spiSendByte(address);
  /* Write LEN bytes */
  for(i=0; i<len; i++)
    spiSendByte(buffer[i]);

  RADIO_DIS_CS();

  return status;
}


