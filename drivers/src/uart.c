#include "stm32f10x.h"
#include "uart.h"


#define UART_NUM        USART3
#define UART_PERIF      RCC_APB1Periph_USART3
#define UART_GPIO_PERIF RCC_APB2Periph_GPIOB
#define UART_GPIO_PORT  GPIOB
#define UART_GPIO_TX    GPIO_Pin_10
#define UART_GPIO_RX    GPIO_Pin_11
#define UART_IRQ_NUM    USART3_IRQn


#define UART_BUFFER_SIZE_TRANSMIT 256
#define UART_BUFFER_SIZE_TRANSMIT 256

static struct
{
    volatile uint32_t head_transmit;
    volatile uint32_t tail_transmit;
    volatile uint32_t head_receive;
    volatile uint32_t tail_receive;
    volatile uint8_t buffer_transmit[UART_BUFFER_SIZE_TRANSMIT];
    volatile uint8_t buffer_receive[UART_BUFFER_SIZE_TRANSMIT];
}uart;


//
static int32_t uart1_receiveValidBufferSize(void);
static int32_t  uart1_transmitIdleBufferSize(void);
static int32_t  uart1_transmitValidBufferSize(void);
static void     uart1_transmitAByte(void);
static uint8_t  uart1_receivePushToBuffer(uint8_t data);


// 获取发送缓冲区空闲空间大小。
int32_t uart1_transmitIdleBufferSize(void)
{
	if(uart.tail_transmit <= uart.head_transmit)
		return UART_BUFFER_SIZE_TRANSMIT - uart.head_transmit + uart.tail_transmit;
	return uart.tail_transmit - uart.head_transmit - 1; // -1:不可以填满。
}

// 获取发送缓冲区中的数据长度。
int32_t uart1_transmitValidBufferSize(void)
{
	if(uart.tail_transmit <= uart.head_transmit)
		return uart.head_transmit - uart.tail_transmit;
	return uart.head_transmit + (UART_BUFFER_SIZE_TRANSMIT - uart.tail_transmit);
}

// 从发送缓冲区取出一个字节，发送出去。
// 不检查缓冲区是否空，不检查发送状态。
void uart1_transmitAByte(void)
{
    USART_SendData(UART_NUM,uart.buffer_transmit[uart.tail_transmit]);
	uart.tail_transmit ++;
	if(uart.tail_transmit >= UART_BUFFER_SIZE_TRANSMIT)
		uart.tail_transmit = 0;
}

// 获取接收缓冲区中的数据长度。
int32_t uart1_receiveValidBufferSize(void)
{
	if(uart.tail_receive <= uart.head_receive)
		return uart.head_receive - uart.tail_receive;
	return uart.head_receive + (UART_BUFFER_SIZE_TRANSMIT - uart.tail_receive);
}

// 把一个字节放入接收缓冲区。
// 缓冲区满则失败。
// 返回值：{0,1}。
uint8_t uart1_receivePushToBuffer(uint8_t data)
{
    uint32_t newHead = uart.head_receive + 1;
	if(newHead == UART_BUFFER_SIZE_TRANSMIT)
		newHead = 0;
    //
    if(newHead == uart.tail_receive)
        return 1;
    //
    uart.buffer_receive[uart.head_receive] = data;
    uart.head_receive = newHead;
    //
    return 0;
}


void uartInit(uint32_t baudrate)
{
    //
    // 重置串口。
    USART_DeInit(UART_NUM);
    //
    // 设置变量
    uart.head_transmit = 0;
    uart.tail_transmit = 0;
    uart.head_receive = 0;
    uart.tail_receive = 0;
    //
    // 接通时钟。
    RCC_APB1PeriphClockCmd(UART_PERIF, ENABLE);
    RCC_APB2PeriphClockCmd(UART_GPIO_PERIF, ENABLE);
    //
    // 设置端口。
 	GPIO_InitTypeDef pin_param;
  	// 发送引脚，配置为复用输出，刷新频率2MHz。
  	pin_param.GPIO_Pin = UART_GPIO_TX;
 	pin_param.GPIO_Mode = GPIO_Mode_AF_PP;
  	pin_param.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_Init(UART_GPIO_PORT, &pin_param);
  	// 接收引脚，配置浮地输入。
  	pin_param.GPIO_Pin = UART_GPIO_RX;
  	pin_param.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  	GPIO_Init(UART_GPIO_PORT, &pin_param);
    //
    // 配置串口。
    USART_InitTypeDef init_param;
    init_param.USART_BaudRate = baudrate;
    init_param.USART_WordLength = USART_WordLength_8b;
    init_param.USART_StopBits = USART_StopBits_1;
    init_param.USART_Parity = USART_Parity_No;
    init_param.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    init_param.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_Init(UART_NUM,&init_param);
    //
    // 设置中断。
    USART_ClearFlag(UART_NUM,USART_FLAG_TC);
    USART_ITConfig(UART_NUM,USART_IT_TC,ENABLE);
    USART_ITConfig(UART_NUM,USART_IT_RXNE,ENABLE);
    //
    NVIC_InitTypeDef nvicParam;
    nvicParam.NVIC_IRQChannel = UART_IRQ_NUM;
    nvicParam.NVIC_IRQChannelPreemptionPriority = 8;
    nvicParam.NVIC_IRQChannelSubPriority = 8;
    nvicParam.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvicParam);

    USART_ITConfig(UART_NUM, USART_IT_RXNE, ENABLE);

    // 启动串口。
    USART_Cmd(UART_NUM,ENABLE);
}



uint32_t uartPutc(uint8_t ch)
{
	if(uart1_transmitIdleBufferSize() < 1)
	{
		return 1; // 空间不够。
	}
	//
    uart.buffer_transmit[uart.head_transmit] = ch;
    uart.head_transmit++;
    if(uart.head_transmit >= UART_BUFFER_SIZE_TRANSMIT)
        uart.head_transmit = 0;
	//
	// 如果发送没有正在进行，就启动发送。
	if(USART_GetFlagStatus(UART_NUM,USART_FLAG_TXE) == SET)
        uart1_transmitAByte();
	//
	return 0;
}

// 把一段数据放入发送缓冲区。
// 返回值：{0,1}
uint32_t uartPuts(char* buf, uint32_t len)
{
    uint32_t i;
	if(uart1_transmitIdleBufferSize() < len)
	{
		return 1; // 空间不够。
	}
	//
	for(i=0;i<len;i++)
	{
		uart.buffer_transmit[uart.head_transmit] = ((uint8_t *)buf)[i];
		//
		uart.head_transmit ++;
		if(uart.head_transmit >= UART_BUFFER_SIZE_TRANSMIT)
			uart.head_transmit = 0;
		//
	}
	//
	// 如果发送没有正在进行，就启动发送。
	if(USART_GetFlagStatus(UART_NUM,USART_FLAG_TXE) == SET)
        uart1_transmitAByte();
	//
	return 0;
}


// 从接收缓冲区取出数据，返回取出的长度。
// 取出的长度为outputBufferLength和uart1_receiveValidBufferSize()的最小者。
uint32_t uartGets(char* buf, uint32_t len)
{
    uint32_t i;
    // 计算outputBufferLength和uart1_receiveValidBufferSize()的最小值。
    uint32_t returnLength = uart1_receiveValidBufferSize();
    if(len < returnLength)
        returnLength = len;
    //
    // 复制数据，推进指针。
    for(i=0;i<returnLength;i++)
    {
        ((uint8_t *)buf)[i] = uart.buffer_receive[uart.tail_receive];
        //
        uart.tail_receive ++;
        if(uart.tail_receive == UART_BUFFER_SIZE_TRANSMIT)
            uart.tail_receive = 0;
    }
    //
    return returnLength;
}

void USART3_IRQHandler(void)
{
    // 发送完成。
    if(USART_GetFlagStatus(UART_NUM,USART_FLAG_TC) == SET)
    {
        USART_ClearFlag(UART_NUM,USART_FLAG_TC);
        //
        if(uart1_transmitValidBufferSize() > 0)
            uart1_transmitAByte();
    }
    //
    // 接收到数据。
    if(USART_GetFlagStatus(UART_NUM,USART_FLAG_RXNE) == SET)
    {
        USART_ClearFlag(UART_NUM,USART_FLAG_RXNE);
        //
        uint8_t data = (uint8_t)USART_ReceiveData(UART_NUM);
        //
        uart1_receivePushToBuffer(data);
    }
}

