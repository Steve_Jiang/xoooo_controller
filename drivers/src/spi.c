#include "stm32f10x.h"
#include "spi.h"


void spiInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
    SPI_InitTypeDef SPI_InitStructure;

	/* Enable SPI and GPIO clocks */
	RCC_APB2PeriphClockCmd(RADIO_GPIO_SPI_CLK | RADIO_GPIO_CS_PERIF |
                         RADIO_GPIO_CE_PERIF | RADIO_GPIO_IRQ_PERIF, ENABLE);

	/* Enable SPI and GPIO clocks */
	RCC_APB1PeriphClockCmd(RADIO_SPI_CLK, ENABLE);

#ifndef STM32_MINIKIT_48
	/* Configure main clock */
	GPIO_InitStructure.GPIO_Pin = RADIO_GPIO_CLK;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(RADIO_GPIO_CLK_PORT, &GPIO_InitStructure);
#endif

	/* Configure SPI pins: SCK, MISO and MOSI */
	GPIO_InitStructure.GPIO_Pin = RADIO_GPIO_SPI_SCK | RADIO_GPIO_SPI_MOSI | RADIO_GPIO_SPI_MISO;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(RADIO_GPIO_SPI_PORT, &GPIO_InitStructure);

    //
    GPIO_InitStructure.GPIO_Pin = RADIO_GPIO_IRQ;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(RADIO_GPIO_IRQ_PORT, &GPIO_InitStructure);


#ifndef STM32_MINIKIT_48
    // Clock the radio with 16MHz
    RCC_MCOConfig(RCC_MCO_HSE);
#endif

	/* Configure I/O for the Chip select */
	GPIO_InitStructure.GPIO_Pin = RADIO_GPIO_CS;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(RADIO_GPIO_CS_PORT, &GPIO_InitStructure);

    RADIO_DIS_CS();

    GPIO_InitStructure.GPIO_Pin = RADIO_GPIO_CE;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(RADIO_GPIO_CE_PORT, &GPIO_InitStructure);

    RADIO_DIS_CE();

	/* SPI configuration */
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_Mode      = SPI_Mode_Master;
    SPI_InitStructure.SPI_DataSize  = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL      = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA      = SPI_CPHA_1Edge;
    SPI_InitStructure.SPI_NSS       = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8; //9Mhz
    SPI_InitStructure.SPI_FirstBit          = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial     = 7;
    SPI_Init(RADIO_SPI, &SPI_InitStructure);

	/* Enable the SPI  */
	SPI_Cmd(RADIO_SPI, ENABLE);
}

char spiSendByte(char byte)
{
    /* Loop while DR register in not emplty */
    while (SPI_I2S_GetFlagStatus(RADIO_SPI, SPI_I2S_FLAG_TXE) == RESET);

    /* Send byte through the SPI1 peripheral */
    SPI_I2S_SendData(RADIO_SPI, byte);

    /* Wait to receive a byte */
    while (SPI_I2S_GetFlagStatus(RADIO_SPI, SPI_I2S_FLAG_RXNE) == RESET);

    /* Return the byte read from the SPI bus */
    return SPI_I2S_ReceiveData(RADIO_SPI);
}


char spiReceiveByte(void)
{
    return spiSendByte(0x0);
}


