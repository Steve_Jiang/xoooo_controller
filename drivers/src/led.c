#include "stm32f10x_conf.h"
#include "led.h"

#ifndef STM32_MINIKIT_48
#define LED_GPIO_PERIF RCC_APB2Periph_GPIOA
#define LED_GPIO_PORT  GPIOA
#define LED_GPIO_RED GPIO_Pin_0
#else
#define LED_GPIO_PERIF RCC_APB2Periph_GPIOB
#define LED_GPIO_PORT  GPIOB
#define LED_GPIO_RED GPIO_Pin_0
#endif


void ledInit(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    // Enable GPIO
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | LED_GPIO_PERIF, ENABLE);
    //Initialize the LED pins as an output
    GPIO_InitStructure.GPIO_Pin = LED_GPIO_RED;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;

    GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);

    //Turn off the LED
    ledSet(0);
}

void ledSet(int onOff)
{
	if(onOff)
		GPIO_SetBits(LED_GPIO_PORT, LED_GPIO_RED);
	else
		GPIO_ResetBits(LED_GPIO_PORT, LED_GPIO_RED);
}

void ledReverse(void)
{
    LED_GPIO_PORT->ODR ^= LED_GPIO_RED;
}


