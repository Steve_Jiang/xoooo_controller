/************************************************
圆点博士微型四轴飞行器版权和声明：
圆点博士所提供的下列代码仅为使用者提供参考，而圆点博士不对该代码提供任何明示或暗含的担保。
圆点博士不对在使用该代码中可能出现的意外或者损失负责，也不对因使用该代码而引起的第三方索赔负责。
该代码可能会被随时变更，相关信息的更新恕不另行通知。
该代码仅以学习的形式发布，并受版权保护, 未经书面授权, 请勿以商业的目的使用或者修改该代码。
圆点博士对该代码保留最终解释权。
圆点博士微型四轴飞行器QQ群：276721324

Dr.R&D DISCLAIMER
THIS CODE IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY, 
AND WITH NO CLAIM AS TO ITS SUITABILITY FOR ANY PURPOSE. 
WE ARE NOT RESPONSIBLE OR LIABLE FOR ANY DAMAGE OR LOSS CAUSED BY USE OF ANY CONTENT FROM THIS CODE. 
SPECIFICATION MENTIONED IN THIS PUBLICATION IS SUBJECT TO CHANGE WITHOUT NOTICE。
WITHOUT AUTHORIZATION, YOU ARE NOT ALLOWED TO COPY, DUPLICATE, OR MODIFY THIS CODE. 
ALL RIGHTS RESERVED.

************************************************/
#include <stm32f10x.h>
//#include "i2c_lcd.h"
#include "eprintf.h"


#define	Joystick_Address	((u32)0x4001244C)

u16 Joystick_RCVTab[16];
u16 Button_RCVTab[8];
extern u8 bs_ii_gas_value;

void Joystick_Init(void)               
{
		GPIO_InitTypeDef   GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);		//etootle: enable clk
	  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);		//etootle: enable clk
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);		//etootle: enable ADC
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);			//etootle: enable dma
	
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_3|GPIO_Pin_4;	
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;			
  	GPIO_Init(GPIOA, &GPIO_InitStructure); 			
#if 0	
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8|GPIO_Pin_9|GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;	
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;			
		GPIO_Init(GPIOB, &GPIO_InitStructure); 
#endif
}

void Joystick_Configuration(void)	
{
		DMA_InitTypeDef	 DMA_InitStructure;
		ADC_InitTypeDef ADC_InitStructure;  

		DMA_DeInit(DMA1_Channel1);
		DMA_InitStructure.DMA_PeripheralBaseAddr = Joystick_Address;
		DMA_InitStructure.DMA_MemoryBaseAddr =(u32)Joystick_RCVTab;
		DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
		DMA_InitStructure.DMA_BufferSize = 5;
		DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
		DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
		DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
		DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
		DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
		DMA_InitStructure.DMA_Priority = DMA_Priority_High;
		DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
		DMA_Init(DMA1_Channel1, &DMA_InitStructure);
		DMA_Cmd(DMA1_Channel1, ENABLE);					//etootle: Enable DMA1 channel1 

		RCC_ADCCLKConfig(RCC_PCLK2_Div6);  
		ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
		ADC_InitStructure.ADC_ScanConvMode = ENABLE;
		ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
		ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
		ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
		ADC_InitStructure.ADC_NbrOfChannel = 5;
		ADC_Init(ADC1,&ADC_InitStructure);
		ADC_RegularChannelConfig(ADC1,ADC_Channel_0,1,ADC_SampleTime_239Cycles5);
		ADC_RegularChannelConfig(ADC1,ADC_Channel_1,2,ADC_SampleTime_239Cycles5);
		ADC_RegularChannelConfig(ADC1,ADC_Channel_2,3,ADC_SampleTime_239Cycles5);
		ADC_RegularChannelConfig(ADC1,ADC_Channel_3,4,ADC_SampleTime_239Cycles5);
		ADC_RegularChannelConfig(ADC1,ADC_Channel_4,5,ADC_SampleTime_239Cycles5);
		ADC_DMACmd(ADC1,ENABLE);
		ADC_Cmd(ADC1,ENABLE);										//etootle: Enable ADC 

		ADC_ResetCalibration(ADC1);							//etootle: ADC calibration
		while(ADC_GetResetCalibrationStatus(ADC1));
		ADC_StartCalibration(ADC1);
		while(ADC_GetCalibrationStatus(ADC1));
		ADC_SoftwareStartConvCmd(ADC1,ENABLE);	//etootle: ADC start
}

void Joystick_Show(void)	
{
	unsigned char PA0 = 0, PA1 = 0, PA2 = 0, PA3 = 0;

#if 1
	PA0 = (Joystick_RCVTab[0]>>4) & 0xff;
	PA1 = (Joystick_RCVTab[1]>>4) & 0xff;
	PA2 = (Joystick_RCVTab[2]>>4) & 0xff;
	PA3 = (Joystick_RCVTab[3]>>4) & 0xff;
	eprintf("%d\n", PA0);
	eprintf("%d\n", PA1);
	eprintf("%d\n", PA2);
	eprintf("%d\n", PA3);
	eprintf("\n");
#endif
}

unsigned char Joystick_Getdata_PA0(void)
{
	return (Joystick_RCVTab[0]>>4) & 0xff;
}

unsigned char Joystick_Getdata_PA1(void)
{
	return (Joystick_RCVTab[1]>>4) & 0xff;
}

unsigned char Joystick_Getdata_PA2(void)
{
	return (Joystick_RCVTab[2]>>4) & 0xff;
}

unsigned char Joystick_Getdata_PA3(void)
{
	return (Joystick_RCVTab[3]>>4) & 0xff;
}





/**/

