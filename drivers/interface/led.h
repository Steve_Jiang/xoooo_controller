#ifndef _LED_H_
#define _LED_H_

void ledInit(void);
void ledSet(int onOff);
void ledReverse(void);

#endif