#ifndef _UART1_H_
#define _UART1_H_


void    uartInit(uint32_t baudrate);
uint32_t uartPutc(uint8_t ch);
uint32_t uartPuts(char* buf, uint32_t len);
uint32_t uartGets(char* buf, uint32_t len);


#endif
