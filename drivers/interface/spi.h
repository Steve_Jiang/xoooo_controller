#ifndef _SPI_H_
#define _SPI_H_

#define USE_SPI2

#ifdef USE_SPI2
#define RADIO_SPI                 SPI2
#define RADIO_SPI_CLK             RCC_APB1Periph_SPI2
#define RADIO_GPIO_SPI_PORT       GPIOB
#define RADIO_GPIO_SPI_CLK        RCC_APB2Periph_GPIOB
#define RADIO_GPIO_SPI_SCK        GPIO_Pin_13
#define RADIO_GPIO_SPI_MISO       GPIO_Pin_14
#define RADIO_GPIO_SPI_MOSI       GPIO_Pin_15


#define RADIO_GPIO_CS             GPIO_Pin_12
#define RADIO_GPIO_CS_PORT        GPIOB
#define RADIO_GPIO_CS_PERIF       RCC_APB2Periph_GPIOB

#define RADIO_GPIO_CLK            GPIO_Pin_8
#define RADIO_GPIO_CLK_PORT       GPIOA
#define RADIO_GPIO_CLK_PERIF      RCC_APB2Periph_GPIOA

#define RADIO_GPIO_CE             GPIO_Pin_10
#define RADIO_GPIO_CE_PORT        GPIOA
#define RADIO_GPIO_CE_PERIF       RCC_APB2Periph_GPIOA

#define RADIO_GPIO_IRQ            GPIO_Pin_9
#define RADIO_GPIO_IRQ_PORT       GPIOA
#define RADIO_GPIO_IRQ_PERIF      RCC_APB2Periph_GPIOA

#else

#define RADIO_SPI                 SPI1
#define RADIO_SPI_CLK             RCC_APB2Periph_SPI1
#define RADIO_GPIO_SPI_PORT       GPIOA
#define RADIO_GPIO_SPI_CLK        RCC_APB2Periph_GPIOB
#define RADIO_GPIO_SPI_SCK        GPIO_Pin_5
#define RADIO_GPIO_SPI_MISO       GPIO_Pin_6
#define RADIO_GPIO_SPI_MOSI       GPIO_Pin_7


#define RADIO_GPIO_CS             GPIO_Pin_4
#define RADIO_GPIO_CS_PORT        GPIOA
#define RADIO_GPIO_CS_PERIF       RCC_APB2Periph_GPIOA


#define RADIO_GPIO_CE             GPIO_Pin_11
#define RADIO_GPIO_CE_PORT        GPIOA
#define RADIO_GPIO_CE_PERIF       RCC_APB2Periph_GPIOA

#define RADIO_GPIO_IRQ            GPIO_Pin_0
#define RADIO_GPIO_IRQ_PORT       GPIOB
#define RADIO_GPIO_IRQ_PERIF      RCC_APB2Periph_GPIOB

#endif


#define RADIO_EN_CE()		GPIO_SetBits(RADIO_GPIO_CE_PORT,RADIO_GPIO_CE)
#define RADIO_DIS_CE()		GPIO_ResetBits(RADIO_GPIO_CE_PORT,RADIO_GPIO_CE)
#define RADIO_EN_CS()		GPIO_ResetBits(RADIO_GPIO_CS_PORT,RADIO_GPIO_CS)
#define RADIO_DIS_CS()		GPIO_SetBits(RADIO_GPIO_CS_PORT,RADIO_GPIO_CS)
#define spiirqIsHigh()		GPIO_ReadInputDataBit(RADIO_GPIO_IRQ_PORT,RADIO_GPIO_IRQ)


void spiInit(void);
char spiSendByte(char byte);
char spiReceiveByte(void);



#endif
