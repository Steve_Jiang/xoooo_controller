#ifndef _NRF24L01_H_
#define _NRF24L01_H_


int nrfInit(void);
void RX_Mode(void);
void TX_Mode(void);
unsigned char NRF24L01_RxPacket(unsigned char *rxbuf);
unsigned char NRF24L01_TxPacket(unsigned char *txbuf);
unsigned char nrfReadReg(unsigned char address);
unsigned char nrfReadBuff(unsigned char address, unsigned char *buffer, int len);
unsigned char nrfWriteReg(unsigned char address, char value);

unsigned char nrfWriteBuf(unsigned char address, unsigned char *buffer, int len);





#endif
